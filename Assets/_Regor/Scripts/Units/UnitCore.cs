using UnityEngine;

public class UnitCore : MonoBehaviour
{
    public UnitInfo Info;
    public sbyte Health { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        Health = (sbyte)Info.HpStat;
    }

    public void TakeDamage(byte damage)
    {
        Health -= (sbyte)damage;

        if (Health <= 0)
            Die();
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
