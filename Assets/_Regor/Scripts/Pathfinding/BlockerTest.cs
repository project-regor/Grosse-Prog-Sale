using UnityEngine;
using Pathfinding;

public class BlockerTest : MonoBehaviour
{
    public void Start()
    {
        SingleNodeBlocker blocker = GetComponent<SingleNodeBlocker>();

        blocker.BlockAtCurrentPosition();
    }
}