﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;
using TileWorld.OdinSerializer.Utilities;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MoveManager : MonoBehaviour
{
    private readonly Dictionary<Vector3Int, Path> _moveOptions = new Dictionary<Vector3Int, Path>();
    private readonly Queue<Trajectory> _trajectory = new Queue<Trajectory>();

    private Grid _grid;

    private MapTile _highlightedCell = new MapTile {Pos = new Vector3Int(int.MinValue, int.MinValue, int.MinValue)};
    private bool _isMoving;

    private bool _isSelectingTile;
    private Tilemap[] _tilemaps;

    private BlockManager.TraversalProvider _traversalProvider;
    private UnitCore _unit;

    public BlockManager BlockManager;
    public Tile HighlightTile;
    public Tile MoveTile;
    public List<SingleNodeBlocker> Obstacles;

    // Start is called before the first frame update
    private void Start()
    {
        _traversalProvider =
            new BlockManager.TraversalProvider(BlockManager, BlockManager.BlockMode.OnlySelector, Obstacles);

        _grid = FindObjectOfType<Grid>();
        _tilemaps = _grid.GetComponentsInChildren<Tilemap>();
        _unit = GetComponent<UnitCore>();

        DrawPath();
    }

    private void Update()
    {
        CheckCursor();
    }

    private void OnConfirm()
    {
        if (!_isSelectingTile ||
            _highlightedCell.Pos == new Vector3Int(int.MinValue, int.MinValue, int.MinValue)) return;

        Path path = _moveOptions[_highlightedCell.Pos];

        for (byte i = 0; i < path.vectorPath.Count; i++)
        {
            //Vector3 mov = new Vector3(node.x, transform.position.y, node.z) - transform.position;
            Trajectory trajectory = new Trajectory
            {
                Position = new Vector3(path.vectorPath[i].x, ((Vector3) path.path[i].position).y + 1,
                    path.vectorPath[i].z),
                Node = path.path[i]
            };

            print((Vector3) path.path[i].position);

            _trajectory.Enqueue(trajectory);
        }

        _highlightedCell = new MapTile
        {
            Pos = new Vector3Int(int.MinValue, int.MinValue, int.MinValue)
        };

        _isSelectingTile = false;
        StartCoroutine(nameof(Move));
    }

    private void CheckCursor()
    {
        if (!_isSelectingTile) return;

        Plane plane = new Plane(Vector3.up, -2);
        Vector3 mousePos = Input.mousePosition;

        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        bool isHit = plane.Raycast(ray, out float enter);

        if (!isHit) return;

        Vector3 hitPoint = ray.GetPoint(enter);

        GraphNode node = AstarPath.active.graphs.First().GetNearest(hitPoint).node;

        Tilemap tilemap = MapManager.Instance.Tilemaps
            .OrderBy(t => Math.Abs(t.transform.position.y - ((Vector3) node.position).y))
            .First();

        Vector3Int cell = _tilemaps[0].WorldToCell(hitPoint);

        if (!_moveOptions.ContainsKey(cell)) return;

        //On reset la tuile précédemment surlignée
        if (_highlightedCell.Pos != new Vector3Int(int.MinValue, int.MinValue, int.MinValue))
            _highlightedCell.Tilemap.SetTile(_highlightedCell.Pos, MoveTile);

        tilemap.SetTile(cell, HighlightTile);
        _highlightedCell = new MapTile
        {
            Pos = cell,
            Tilemap = tilemap
        };
    }

    private IEnumerator Move()
    {
        if (_trajectory.Count == 0) yield break;
        Vector3 pos = _trajectory.First().Position;

        float timeSinceStarted = 0f;
        while (true)
        {
            if (_trajectory.Count == 0) yield break;
            timeSinceStarted += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, pos, timeSinceStarted);

            // If the object has arrived, stop the coroutine
            if (transform.position == pos)
            {
                _trajectory.Dequeue();
                if (_trajectory.Count == 0)
                {
                    transform.position = new Vector3(
                        Mathf.Round(transform.position.x),
                        (float) Math.Round(transform.position.y, 1),
                        Mathf.Round(transform.position.z));

                    DrawPath();
                    yield break;
                }

                yield return StartCoroutine(nameof(Move));
            }

            // Otherwise, continue next frame
            yield return null;
        }
    }

    private void DrawPath()
    {
        _tilemaps.ForEach(t => t.ClearAllTiles());
        _moveOptions.Clear();
        byte tileSize = MapManager.Instance.TileSize;

        Vector3Int curPos = _tilemaps[0].WorldToCell(transform.position);

        //On test tous les mouvements possibles autour du personnage pour déterminer ceux qui sont légal
        for (int x = (int) transform.position.x - _unit.Info.MoveStat * tileSize - 1;
            x < (int) transform.position.x + _unit.Info.MoveStat * tileSize + 1;
            x += tileSize)
        for (int z = (int) transform.position.z - _unit.Info.MoveStat * tileSize - 1;
            z < (int) transform.position.z + _unit.Info.MoveStat * tileSize + 1;
            z += tileSize)
        {
            GraphNode node = AstarPath.active.graphs.First().GetNearest(new Vector3(x, transform.position.y, z))
                .node;

            Tilemap tilemap = MapManager.Instance.Tilemaps
                .OrderBy(t => Math.Abs(t.transform.position.y - ((Vector3) node.position).y))
                .First();

            Path path = ABPath.Construct(transform.position, new Vector3(x, 0, z));
            path.traversalProvider = _traversalProvider;

            AstarPath.StartPath(path);
            path.BlockUntilCalculated();

            if (path.vectorPath.Count > _unit.Info.MoveStat + 1 || !path.vectorPath.Any()) continue;

            Vector3Int cell = tilemap.WorldToCell(path.vectorPath.Last());
            if (cell == curPos) continue;

            tilemap.SetTile(cell, MoveTile);
            if (!_moveOptions.ContainsKey(cell))
                _moveOptions.Add(cell, path);
        }

        _isSelectingTile = true;
    }

    private sealed class Trajectory
    {
        public GraphNode Node;
        public Vector3 Position;
    }

    private sealed class MapTile
    {
        public Vector3Int Pos;
        public Tilemap Tilemap;
    }
}