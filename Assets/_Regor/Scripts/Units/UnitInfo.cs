using UnityEngine;

[CreateAssetMenu(fileName = "New Unit", menuName = "Battle Entity/Unit")]
public class UnitInfo : ScriptableObject
{
    public enum AllegianceEnum
    {
        Player,
        Enemy
    }

    public string Name;
    public byte HpStat;
    public byte AtkStat;
    public byte DefStat;
    public byte DexStat;
    public byte SpeedStat;
    public byte MoveStat;
    public byte JumpStat;
    public AllegianceEnum Allegiance;
}